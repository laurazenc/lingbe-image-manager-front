(function() {
  'use strict';
  angular
  .module('lingbe-im')
  .controller('CategoryController', CategoryController);

  CategoryController.$inject = ['$scope', '$state', 'ImageService', 'CategoryService'];
  function CategoryController($scope, $state, ImageService, CategoryService) {

    $scope.category = {_id: $state.params.categoryId, title: $state.params.categoryTitle};

    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    CategoryService.getCategory($scope.category)
    .then(function(data) {
      $scope.images = formatData(data.data.result);
    },function(err) {
      console.log(err);
    })

    function formatData(images) {
      var formatedData = [];
      angular.forEach(images, function(image, key) {
        var location = JSON.parse(image.geoLocation);
        image.geoLocation = location;
        formatedData.push(image);
      });
      return formatedData;
    }
  }

}());
