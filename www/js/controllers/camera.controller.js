(function() {
  'use strict';

  angular
  .module('lingbe-im')
  .controller('CameraController', CameraController);

  CameraController.$inject = ['$scope', '$cordovaDevice', '$cordovaFile',
                              '$ionicPlatform', '$ionicActionSheet', 'ImageService',
                              'FileManagerService', '$state', 'CategoryService',
                              'GeolocationService'];

  function CameraController($scope, $cordovaDevice, $cordovaFile, $ionicPlatform,
                            $ionicActionSheet, ImageService, FileManagerService, $state, CategoryService,
                            GeolocationService) {

    $ionicPlatform.ready(function() {
      $scope.map = null;
      $scope.imageData = {
        selectedCategory: null,
        categories: [],
        newImage: null
      }
      $scope.saveImage       = saveImage;
      $scope.getFake         = false;
      $scope.getFakeLocation = getFakeLocation;
      $scope.image           = {}
      $scope.geoMessage      = 'Getting GPS location ...';

      $scope.show = function() {
        var hideSheet = $ionicActionSheet.show({
          buttons: [
            {text: '<i class="icon ion-ios-camera"></i> Take photo'},
            {text: '<i class="icon ion-ios-folder"></i>Photo from library'}
          ] ,
          titleText: 'Add Images',
          cancelText: 'Cancel',
          cancel: function() {
            $state.go('dashboard');
            return true;
          },
          buttonClicked: function(index) {
            $scope.addImage(index);
            return true;
          }
        });
      }

      $scope.show();

      CategoryService.getCategories()
      .then(function(categories) {
        $scope.imageData.categories = categories;
      });

      function saveImage() {
        if($scope.imageData.selectedCategory) {
          var image = {
            category: $scope.imageData.selectedCategory,
            imageBase64: $scope.imageData.newImage,
            location: $scope.imageData.location
          };

          ImageService.saveImage(image)
          .then(function() {
            // TODO toast created
            $state.go('dashboard');
          })
          .catch(function(err) {
            console.log(err);
          });

        }
      }

      $scope.addImage = function(type) {
        ImageService.handleMedia(type)
        .then(function(image) {
          $scope.imageData.newImage = image;
        })
        .then(getGeolocation())
        .catch(function(err) {
          console.log('Error', err);
        });
      }
    });

    function getGeolocation() {
      GeolocationService.getCurrentLocation()
      .then(function(location) {
        console.log(location);
      }, function() {
        $scope.fakeMessage = 'GPS might be not working since this is in dev mode. Just for testing purpose let\'s get a fake location' ;
        $scope.getFake = true;
      });
    }

    function getFakeLocation() {
      $scope.imageData.location = GeolocationService.getFakeLocation({latitude:40.4381311,longitude:-3.8196221}, 2000);
      $scope.map = { lat: $scope.imageData.location.lat, long: $scope.imageData.location.long , zoom: 11, name: $scope.imageData.location.name };
      $scope.getFake = false;
    }

  }
})();
