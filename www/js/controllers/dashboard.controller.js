(function() {
  'use strict';
  angular
  .module('lingbe-im')
  .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$scope', 'ImageService', '$ionicPlatform', 'CategoryService', '$q', '$ionicModal'];

  function DashboardController($scope, ImageService, $ionicPlatform, CategoryService, $q, $ionicModal) {

    $scope.categories = [];
    $scope.newCategory = '';
    $scope.openCategoryModal = openCategoryModal;
    $scope.createCategory = createCategory;
    $scope.getCategories = getCategories;

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
      getCategories();
      $scope.openModal();
    };

    function getCategories() {
      CategoryService.getCategories()
      .then(function(categories) {
        $scope.categories = categories;

        return ImageService.getPromiseChain(categories);
      })
      .then(function(data) {
        angular.forEach(data, function(images) {
          if(images.data.result.length > 0) {
            angular.forEach($scope.categories, function(category, key) {
              if(images.data.result[0].category === category._id) {
                $scope.categories[key].images = images.data.result;
              }
            });
          }
        });
      })
      .catch(function(err) {
        console.log(err);
      })
      .finally(function() {
         // Stop the ion-refresher from spinning
         $scope.$broadcast('scroll.refreshComplete');
       });
    }

    $scope.openModal = function() {
      $ionicModal.fromTemplateUrl('category-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.newCategory = '';
      });
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    function openCategoryModal() {
      console.log($scope.modal);
      $scope.modal.show();
    }
    function getImagesForCategory(categories) {
      console.log(categories);
      console.log($scope.categories);
    }

    function createCategory() {
        var category = {title: $scope.modal.newCategory};
        CategoryService.createCategory(category)
        .then(function() {
          $scope.modal.hide();
          getCategories();
        }, function() {
          console.log('error');
        })
    }
  }
}());
