(function() {
  'use strict';

  angular
  .module('lingbe-im')
  .factory('GeolocationService', GeolocationService);

  GeolocationService.$inject = ['$cordovaGeolocation', '$q'];

  function GeolocationService($cordovaGeolocation, $q) {
      return {
        getCurrentLocation: getCurrentLocation,
        getFakeLocation: getFakeLocation
      }



      function getCurrentLocation() {
        var posOptions = {
          enableHighAccuracy: false,
          timeout: 5000
        };
        return $q(function(resolve, reject) {
          $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;

            console.log(lat)
            console.log(long);
            resolve({lat: lat, long: long});

          }, function(err) {
            reject();
          });
        })
      }
      function getFakeLocation() {
        var location = [
        {'name': 'Oxford, England', 'lat': 51.73213, 'long': -1.20631},
        {'name': 'Quito, Ecuador', 'lat': -0.2333, 'long': -78.5167},
        {'name': 'Ushuaia, Argentina', 'lat': -54.8000, 'long': -68.3000},
        {'name': 'McMurdo Station, Antartica', 'lat': -77.847281, 'long': 166.667942},
        {'name': 'Norilsk, Siberia', 'lat': 69.3333, 'long': 88.2167},
        {'name': 'Greenwich, England', 'lat': 51.4800, 'long': 0.0000},
        {'name': 'Suva, Fiji', 'lat': -18.1416, 'long': 178.4419},
        {'name': 'Tokyo, Japan', 'lat': 35.6833, 'long': 139.6833},
        {'name': 'Mumbai, India', 'lat': 18.9750, 'long': 72.8258},
        {'name': 'New York, USA', 'lat': 40.7127, 'long': -74.0059},
        {'name': 'Moscow, Russia', 'lat': 55.7500, 'long': 37.6167},
        {'name': 'Cape Town, South Africa', 'lat': -33.9253, 'long': 18.4239},
        {'name': 'Cairo, Egypt', 'lat': 30.0500, 'long': 31.2333},
        {'name': 'Sydney, Australia', 'lat': -33.8650, 'long': 151.2094} ];

        return location[Math.round(Math.random()*location.length) + 1];

      }
  }
}());
