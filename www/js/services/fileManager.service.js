(function() {
  'use strict';

  angular
  .module('lingbe-im')
  .factory('FileManagerService', FileManagerService);

  FileManagerService.$inject = ['$http', '$q', 'API_URL'];

  function FileManagerService($http, $q, API_URL) {
    var images;
    var IMAGE_STORAGE_KEY = 'lingbe-im';
    return {
      storeImage: storeImage,
      getImages: getImages
    }

    function getImages() {
      // var img = window.localStorage.getItem(IMAGE_STORAGE_KEY);
      // console.log('img', img);
      // imames = (img) ? // if(img) {
      //   images = JSON.parse(img);
      // } else {
      //   images = [];
      // }
      // return $q(function(resolve, reject) {
      //   $http.get(API_URL + 'images/');
      // }

      return images;
    }

    function storeImage(img) {
      console.log('storeImage', img);
      images.push(img.name);
      // images.push()
      // window.localStorage.setItem(IMAGE_STORAGE_KEY, JSON.stringify(images));
    }
  }
}());
