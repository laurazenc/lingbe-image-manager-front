(function() {
  'use strict';

  angular
  .module('lingbe-im')
  .factory('ImageService', ImageService);

  ImageService.$inject = ['$cordovaCamera', '$q', '$cordovaGeolocation', 'API_URL',  '$http'];

  function ImageService($cordovaCamera, $q, $$cordovaGeolocation, API_URL, $http) {
      return {
        handleMedia: handleMedia,
        saveImage: saveImage,
        getImagesForCategory: getImagesForCategory,
        getPromiseChain: getPromiseChain
      };

    function optionsForType(type) {
      var source;
      switch(type) {
        case 0:
          source = Camera.PictureSourceType.CAMERA;
          break;
        case 1:
          source = Camera.PictureSourceType.PHOTOLIBRARY;
          break;
      }

      return {
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: source,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        targetWidth: 500,
        targetHeight: 500
      }
    }

    function handleMedia(type) {
      return $q(function(resolve, reject) {
        var options = optionsForType(type);
        var lat, long;
        $cordovaCamera.getPicture(options).then(function(imageData) {
          resolve("data:image/jpeg;base64," + imageData);
        }, function(err) {
          console.log(err);
          reject();
        });
      });
    }

    function saveImage(image) {
      return $q(function(resolve, reject){
        $http.post(API_URL.url + 'images',{params: image})
        .then(function(data) {
          resolve(data.data.msg);
        }, function(err) {
          reject(err);
        });
      })
    }

    function getImagesForCategory(category) {
      return $q(function(resolve, reject){
        $http.get(API_URL.url + 'images/' +  category._id)
        .then(function(data) {
          resolve(data);
        }, function(err) {
          reject(err);
        });
      });
    }

    function getPromiseChain(categories) {
      var promises = [];
      angular.forEach(categories, function(category) {
        promises.push(getImagesForCategory(category));
      })

      return $q.all(promises);

    }

  }
})();
