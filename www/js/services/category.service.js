(function() {
  'use strict';
  angular
  .module('lingbe-im')
  .factory('CategoryService', CategoryService);

  CategoryService.$inject = ['API_URL', '$q', '$http'];
  function CategoryService(API_URL, $q, $http) {
    return {
      getCategories: getCategories,
      getCategory: getCategory,
      createCategory: createCategory,
      editCategory: editCategory,
      deleteCategory: deleteCategory
    }

    function getCategories() {
      return $q(function(resolve, reject) {
        $http.get(API_URL.url + 'categories')
        .then(function(categories) {

          resolve(categories.data.result);
        })
        .catch(function(err) {
          reject(err);
        });
      });
    }
    function getCategory(category) {
      return $q(function(resolve, reject) {
        $http.get(API_URL.url+ 'category/' + category._id)
        .then(function(data) {
          resolve(data);
        }, function() {
          reject();
        });
      })
    }
    function createCategory(category) {

      return $q(function(resolve, reject) {
        $http.post(API_URL.url+ 'categories', {params: category})
        .then(function(data) {
          resolve();
        }, function() {
          reject();
        });
      })
    }
    function editCategory() {
    }
    function deleteCategory() {
    }
  }
}());
