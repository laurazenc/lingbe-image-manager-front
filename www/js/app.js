(function() {
  'use strict';
  angular.module('lingbe-im', [
    'ionic',
    'ngCordova',
    'lingbe-im',
    'angularMoment',
    'ngMap'
  ])

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })
  .constant('API_URL', {
    url: 'http://192.168.1.38:8100/api/'
  })
  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state('dashboard', {
      url: '/',
      templateUrl: 'templates/dashboard.html',
      controller: 'DashboardController'
    })

    .state('new-image', {
      url: '/new-image',
      templateUrl: 'templates/new-image.html',
      controller: 'CameraController'
    })
    .state('category', {
      url: '/category?categoryId&categoryTitle',
      templateUrl: 'templates/category.html',
      controller: 'CategoryController'
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');

  });
  
}());
