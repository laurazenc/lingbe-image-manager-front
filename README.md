### Installation

```
npm install -g ionic cordova
npm install
bower install
gulp watch
ionic run android -l -c -s
```
### CORS problem
To avoid cors problems change the origin url constant to what your device is running from.

The proxy url is where the server is running.

app.js

```
.constant("API_URL", {
  url: "http://192.168.1.38:8100/api/"
  })

```

ionic.config
```
"proxies": [
  {
    "path": "/api",
    "proxyUrl": "http://localhost:8080/api"
  }
]
```